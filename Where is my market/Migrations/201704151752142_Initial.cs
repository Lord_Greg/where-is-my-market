namespace Where_is_my_market.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChainOfStores",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Shops",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        StreetName = c.String(nullable: false),
                        StreetNo = c.String(nullable: false),
                        Longitude = c.Double(),
                        Latitude = c.Double(),
                        ChainID = c.Int(nullable: false),
                        CityID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ChainOfStores", t => t.ChainID, cascadeDelete: true)
                .ForeignKey("dbo.Cities", t => t.CityID)
                .Index(t => t.ChainID)
                .Index(t => t.CityID);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Shops", "CityID", "dbo.Cities");
            DropForeignKey("dbo.Shops", "ChainID", "dbo.ChainOfStores");
            DropIndex("dbo.Shops", new[] { "CityID" });
            DropIndex("dbo.Shops", new[] { "ChainID" });
            DropTable("dbo.Cities");
            DropTable("dbo.Shops");
            DropTable("dbo.ChainOfStores");
        }
    }
}

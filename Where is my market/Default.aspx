﻿<%@ Page Title="Strona Główna" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Where_is_my_market._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Where is my market?</h1>
        <p class="lead">Aplikacja służy do gromadzenia, oraz wyszukiwania lokalizacji popularnych sklepów sieciowych, tak zwanych "marketów".</p>

        <br />
        <h2>Sklepy blisko Ciebie:</h2>
        <div id="map" style="height: 500px; width: 100%;"></div>
    </div>

    <script>
        var map, infoWindow;

        function initMap() {
            map = new google.maps.Map(document.getElementById("map"), {
                center: { lat: 53.1050130, lng: 18.0292030 },
                zoom: 15
            });
            infoWindow = new google.maps.InfoWindow;

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var userLocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    var marker = new google.maps.Marker({
                        position: userLocation,
                        map: map,
                        title: "Tu jesteś"
                    });

                    infoWindow.setPosition(userLocation);
                    infoWindow.setContent("<h4>Twoja Lokalizacja</h4>");
                    
                    marker.addListener('click', function () {
                        infoWindow.open(map, marker);
                    });

                    map.setCenter(userLocation);
                    
                    addShopsMarkers(map, userLocation, 2.0);

                }, function() {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            }
            else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
            }
        }

        function handleLocationError(browserHasGeolocation, infoWindow, userLocation) {
            infoWindow.setPosition(userLocation);
            infoWindow.setContent(browserHasGeolocation ?
                'Ups! Wystąpił błąd podczas ustalania twojej lokalizacji.' :
                'Ups! Twoja przeglądarka nie wspiera geolokalizacji.');
            
            infoWindow.open(map);
        }


        function addShopsMarkers(map, userLocation, distance) {
            $.ajax({
                type: "POST",
                url: "Default.aspx/GetShopsNearby",
                data: "{ userLat: " + userLocation.lat + ", userLng:" + userLocation.lng + ", distance: " + distance + " }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var locations = JSON.parse(response.d);
                    
                    for (var i in locations) {
                        var street = locations[i].StreetName + " " + locations[i].StreetNo;

                        var color = getColorString(locations[i].Chain.MarkerColorRgb.toString(16));
                        var markerImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + color);

                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(locations[i].Latitude, locations[i].Longitude),
                            map: map,
                            icon: markerImage,
                            title: locations[i].Chain.Name + ": " + street
                        });

                        var infowindow = new google.maps.InfoWindow();
                        var content = "<h4>" + locations[i].Chain.Name + "</h4><p>ul. " + street + "</p>";
                        google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                            return function () {
                                infowindow.setContent(content);
                                infowindow.open(map, marker);
                            };
                        })(marker, content, infowindow));
                    }
                }
            });
        }


        function getFloat(text){
            return parseFloat(text.replace(',', '.'));
        }

        function getColorString(hexStr){
            var colorStr = hexStr;
            
            if(colorStr.length > 6){
                colorStr = colorStr.substring(colorStr.length - 7, colorStr.length - 1);
            }
            else{
                while(colorStr.length < 6){
                    colorStr = "0" + colorStr;
                }
            }

            return colorStr
        }
    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCTXSw99FG7kQ5YBlCmIAfMtcCOQYocctc&callback=initMap"></script>
    
</asp:Content>

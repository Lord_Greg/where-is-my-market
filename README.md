# Where is my market? #
Aplikacja służąca do wyszukiwania popularnych w Polsce sklepów sieciowych (tzw. "marketów") w twojej okolicy. Baza sklepów jest aktualizowana raz w miesiącu.

### Lista obecnie obsługiwanych sieci: ###
* Auchan
* Biedronka
* Carrefour
* Lidl
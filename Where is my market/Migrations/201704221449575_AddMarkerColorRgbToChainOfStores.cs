namespace Where_is_my_market.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMarkerColorRgbToChainOfStores : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChainOfStores", "MarkerColorRgb", c => c.Int(nullable: false, defaultValue: 0xFE7569));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChainOfStores", "MarkerColorRgb");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using Where_is_my_market.DAL;
using Where_is_my_market.Models;

namespace Where_is_my_market
{
    public partial class ChainsPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        public List<ChainOfStores> GetChainsOfStores()
        {
            List<ChainOfStores> chains;

            using (ShopContext db = new ShopContext())
            {
                chains = db.ChainsOfStores.ToList();
            }

            return chains;
        }
    }
}
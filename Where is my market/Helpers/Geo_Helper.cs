﻿using System.Device.Location;

namespace Where_is_my_market.Helpers
{
    public class GeoHelper
    {
        /// <summary>
        /// Gets coordinates of vertex of a square with given center point and sides in given distance from it.
        /// </summary>
        /// <param name="centerPoint">The center of a square.</param>
        /// <param name="distance">Distance between square's center and it's side, given in kilometers.</param>
        /// <returns>Two element array that contain north-west vertex of a square in 0 index, and south-east one in 1 index.</returns>
        public static GeoCoordinate[] GetDistanceBorderPoints(GeoCoordinate centerPoint, double distance)
        {
            // The main idea of this algorithm is to find border points of the given distance in possibly the fastest way.
            // It's made for small distances comparing to the length of Earth, so we don't really need too mutch precision.
            //
            //  -------
            // |       |
            // |   X   | ^
            // |       | | - distance
            //  -------  v
            //     <--> - distance
            // X - centerPoint = (lng, lat)
            // Length of Earth =~ 40 000 km = 360 dgrs
            // 1 km = 0,009 dgrs
            // 
            // Then:
            // NW border = (lng - 0,009*distance, lat + 0,009*distance)
            // SE border = (lng + 0,009*distance, lat - 0,009*distance)

            GeoCoordinate[] borderPoints = new GeoCoordinate[2];
            double dgrsDistance = 0.009 * distance;

            borderPoints[0] = new GeoCoordinate(centerPoint.Latitude + dgrsDistance, centerPoint.Longitude - dgrsDistance);
            borderPoints[1] = new GeoCoordinate(centerPoint.Latitude - dgrsDistance, centerPoint.Longitude + dgrsDistance);

            return borderPoints;
        }
    }
}

﻿using HtmlAgilityPack;
using SharpKml.Dom;
using SharpKml.Engine;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using Where_is_my_market.Helpers;
using Where_is_my_market.Models;

namespace Where_is_my_market.Tasks
{
    public class WebScraper
    {
        public int GetBiedronkaShops()
        {
            int chainID = 1; // Defined on BD initialization
            int actualizedCount = 0;
            int currentPage = 0;
            int maxKnownPage = 0;
            string urlScafford = "http://www.biedronka.pl/pl/sklepy/lista,page,";
            string htmlCode;
            HtmlDocument htmlDoc = new HtmlDocument();
            List<Shop> newShops = new List<Shop>();

            do
            {
                currentPage++;

                try
                {
                    htmlCode = GetPage(urlScafford + currentPage.ToString());
                    htmlDoc.LoadHtml(htmlCode);

                    maxKnownPage = Int32.Parse(
                        htmlDoc
                            .DocumentNode
                            .SelectNodes("//*[@id=\"container\"]/div[2]/div/article/div/ul/li")
                            .Last()
                            .SelectSingleNode("./a/text()")
                            .InnerText
                    );

                    foreach (HtmlNode node in htmlDoc.DocumentNode.SelectNodes("//*[@id=\"container\"]/div[2]/div/article/ul/li"))
                    {
                        string[] street = node.SelectSingleNode("./h4/span/span/text()").InnerText.Split(' ');

                        newShops.Add(new Shop(
                            chainID: chainID,
                            cityName: StringHelper.BuildString(node.SelectSingleNode("./h4/text()").InnerText.Trim().Split(' ')),
                            streetName: StringHelper.BuildString(street.Take(street.Count() - 1).ToList<string>()),
                            streetNo: street.Last()
                        ));
                    }
                }
                catch
                {
                    maxKnownPage = 0;
                }

            } while (currentPage < maxKnownPage || currentPage > 200); // In the case of error and infinity loop

            actualizedCount = Shop.ActualizeDb(chainID, newShops);

            return actualizedCount;
        }

        public int GetCarrefourShops()
        {
            int chainID = 2; // Defined on BD initialization
            int actualizedCount = 0;
            string url = "https://satysfakcja.carrefour.pl/PanelStores/tabid/273/Default.aspx";
            HtmlDocument htmlDoc = new HtmlDocument();
            List<Shop> newShops = new List<Shop>();

            htmlDoc.LoadHtml(GetPage(url));

            foreach (HtmlNode node in htmlDoc.DocumentNode.SelectNodes("//*[@id=\"dnn_ctr898_HtmlModule_lblContent\"]/ol/li"))
            {
                string[] localization = node.InnerText.Split(',');
                string[] street = localization.Last().Split(' ');

                newShops.Add(new Shop(
                    chainID: chainID,
                    cityID: City.FindOrCreate(localization.First(), withoutPolishCharacters: true).ID,
                    streetName: StringHelper.BuildString(street.Take(street.Count() - 1).ToList<string>()),
                    streetNo: street.Last()
                ));
            }

            actualizedCount = Shop.ActualizeDb(chainID, newShops);

            return actualizedCount;
        }

        public int GetAuchanShops()
        {
            int chainID = 3; // Defined on BD initialization
            int actualizedCount = 0;
            string url = "http://www.auchan.pl/nasze-sklepy/mapa-sklepow-auchan";
            HtmlDocument htmlDoc = new HtmlDocument();
            List<Shop> newShops = new List<Shop>();

            htmlDoc.LoadHtml(GetPage(url));

            foreach (HtmlNode node in htmlDoc.DocumentNode.SelectNodes("//*[@id=\"shops-list\"]/div[1]/div[2]/p"))
            {
                HtmlDocument htmlDocDetails = new HtmlDocument();
                htmlDocDetails.LoadHtml(GetPage("http://www.auchan.pl" + node.FirstChild.Attributes["href"].Value));

                HtmlNodeCollection addressNodes = htmlDocDetails
                    .DocumentNode
                    .SelectNodes("//*[@id=\"content\"]/div/div[3]/div[2]/div[1]/div/p"); // There's a jQuery that change id="content" into id="content-2"

                if(addressNodes == null)
                {
                    addressNodes = htmlDocDetails
                        .DocumentNode
                        .SelectNodes("//*[@id=\"shop\"]/div[2]/div[1]/text()[normalize-space(.) != '']");
                }

                List<string> street = addressNodes
                    .First()
                    .InnerText
                    .Trim()
                    .Split(' ')
                    .ToList();
                street.RemoveAll(s => s == "");

                if (street.First() == "ul.")
                {
                    street.RemoveAt(0);
                }

                List<string> cityData = addressNodes
                    .Last()
                    .InnerText
                    .Trim()
                    .Split(' ')
                    .ToList();
                cityData.RemoveAll(data => data == "");
                cityData.RemoveAt(0); // Remove postal code

                newShops.Add(new Shop(
                    chainID: chainID,
                    cityName: StringHelper.BuildString(cityData),
                    streetName: StringHelper.BuildString(street.Take(street.Count() - 1).ToList<string>()),
                    streetNo: street.Last()
                ));
            }

            actualizedCount = Shop.ActualizeDb(chainID, newShops);

            return actualizedCount;
        }

        public int GetLidlShops()
        {
            int chainID = 5; // Defined on BD initialization
            int actualizedCount = 0;
            string url = "http://www.poi-service.de/lidl/daten/google_earth.zip";
            string archiveName = "lidl_package.zip";
            string archivePath = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + archiveName;
            string listFileName = "Google.kml";
            string kmlData;
            List<Shop> newShops = new List<Shop>();
            
            using (WebClient client = new WebClient())
            {
                client.DownloadFile(url, archivePath);
            }

            using(ZipArchive archive = ZipFile.Open(archivePath, ZipArchiveMode.Read))
            {
                kmlData = new StreamReader(
                    stream: archive
                        .Entries
                        .Where(file => file.Name == listFileName)
                        .First()
                        .Open(),
                    encoding: Encoding.UTF8
                ).ReadToEnd();
            }

            if (File.Exists(archivePath))
            {
                File.Delete(archivePath);
            }

            kmlData = kmlData.Replace('&', 'i'); // Replace character causing error
            KmlFile kmlDoc = KmlFile.Load(new StringReader(kmlData));

            foreach(Placemark placemark in kmlDoc.Root.Flatten().OfType<Placemark>().Where(place => IsInPoland(place.Geometry as Point)))
            {
                List<string> city = placemark.Name.Split(' ').ToList();
                city.RemoveAt(0); // Remove "Lidl" world

                string[] street = StringHelper.SubstringBetween(placemark.Description.Text, "", "<br>").Split(' ');

                newShops.Add(new Shop(
                    chainID: chainID,
                    cityID: City.FindOrCreate(StringHelper.BuildString(city), withoutPolishCharacters: true).ID,
                    streetName: StringHelper.BuildString(street.Take(street.Count() - 1).ToList<string>()),
                    streetNo: street.Last()
                ));
            }

            actualizedCount = Shop.ActualizeDb(chainID, newShops);

            return actualizedCount;
        }



        private string GetPage(string url)
        {
            string htmlCode;

            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                htmlCode = client.DownloadString(url);
            }

            return htmlCode;
        }

        private bool IsInPoland(Point point)
        {
            // Borders of Poland:
            // North:   54.84 N
            // West:    14.10 E
            // East:    24.15 E
            // South:   49.00 N

            if (point.Coordinate.Latitude > 49.00 && point.Coordinate.Latitude < 54.84 && 
                point.Coordinate.Longitude > 14.10 && point.Coordinate.Longitude < 24.15)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

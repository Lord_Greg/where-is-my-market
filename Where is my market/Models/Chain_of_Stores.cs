﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;

namespace Where_is_my_market.Models
{
    public class ChainOfStores
    {
        [ScaffoldColumn(false), JsonIgnore]
        public int ID { get; set; }

        [Required, Display(Name = "Nazwa")]
        public string Name { get; set; }

        [Display(Name = "Kolor znacznika na mapie")]
        public Int32 MarkerColorRgb
        {
            get
            {
                return MarkerColor.R * 256 * 256 + MarkerColor.G * 256 + MarkerColor.B;
            }
            set
            {
                MarkerColor = ColorTranslator.FromHtml("#" + value.ToString("X"));
            }
        }


        [JsonIgnore]
        public virtual ICollection<Shop> Shops { get; set; }

        [NotMapped, JsonIgnore]
        public Color MarkerColor { get; set; }



        public ChainOfStores()
        {
            MarkerColor = ColorTranslator.FromHtml("#" + 0xFE7569.ToString("X"));
        }
    }
}

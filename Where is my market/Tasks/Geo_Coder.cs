﻿using System;
using System.Device.Location;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Where_is_my_market.Tasks
{
    public class GeoCoder
    {
        public static GeoCoordinate GetGeoCode(string address, string region = "pl")
        {
            GeoCoordinate coordinate = new GeoCoordinate();
            string response;
            string url = string.Format(
                "https://maps.googleapis.com/maps/api/geocode/json?address={0}&region={1}&key={2}",
                args: new string[] {
                    Uri.EscapeDataString(address),
                    region,
                    "AIzaSyB4-tS_voi5wIikKUWQZQAIlpzSx9wVyuk" // API key
                });

            using (WebClient client = new WebClient())
            {
                response = client.DownloadString(url);
            }

            XmlDictionaryReader jsonReader = JsonReaderWriterFactory.CreateJsonReader(
                Encoding.UTF8.GetBytes(response),
                new XmlDictionaryReaderQuotas()
            );
            XElement root = XElement.Load(jsonReader);

            coordinate.Latitude = double.Parse(
                root.XPathSelectElement("//results/item/geometry/location/lat").Value,
                System.Globalization.CultureInfo.InvariantCulture
            );
            coordinate.Longitude = double.Parse(
                root.XPathSelectElement("//results/item/geometry/location/lng").Value,
                System.Globalization.CultureInfo.InvariantCulture
            );
            
            return coordinate;
        }
    }
}

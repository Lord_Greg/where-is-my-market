﻿using System.Data.Entity;
using System.Linq;
using Where_is_my_market.Models;

namespace Where_is_my_market.DAL
{
    public class ShopContext : DbContext
    {
        public ShopContext() : base("ShopContext")
        {
            Database.SetInitializer<ShopContext>(null);
        }

        public DbSet<City> Cities { get; set; }
        public DbSet<ChainOfStores> ChainsOfStores { get; set; }
        public DbSet<Shop> Shops { get; set; }

        public IQueryable<ChainOfStores> GetChainsOfStores(bool includeRelations = false)
        {
            IQueryable<ChainOfStores> query = ChainsOfStores;

            if (includeRelations)
            {
                query = query.Include(chain => chain.Shops);
            }

            return query;
        }

        public IQueryable<City> GetCities(bool includeRelations = false)
        {
            IQueryable<City> query = Cities;

            if (includeRelations)
            {
                query = query.Include(city => city.Shops);
            }

            return query;
        }

        public IQueryable<Shop> GetShops(bool includeRelations = false)
        {
            IQueryable<Shop> query = Shops;

            if (includeRelations)
            {
                query = query.Include(shop => shop.Chain).Include(shop => shop.City);
            }

            return query;
        }
    }
}
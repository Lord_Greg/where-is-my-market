﻿<%@ Page Title="Sieci Sklepów" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Chains.aspx.cs" Inherits="Where_is_my_market.ChainsPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Wspierane Sieci Sklepów</h1>
    
    <asp:ListView ID="chainsList" runat="server"
        DataKeyNames="ID"
        ItemType="Where_is_my_market.Models.ChainOfStores"
        SelectMethod="GetChainsOfStores">
        
        <ItemTemplate>
            <li><%#:Item.Name%></li>
        </ItemTemplate>
    </asp:ListView>
    
</asp:Content>

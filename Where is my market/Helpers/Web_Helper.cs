﻿using Newtonsoft.Json;

namespace Where_is_my_market.Helpers
{
    public class WebHelper
    {
        /// <summary>
        /// Serialize input <see cref="object"/> to a JSON string.
        /// </summary>
        /// <param name="obj">Object to serialize.</param>
        /// <returns>JSON string.</returns>
        public static string ObjectToJson(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
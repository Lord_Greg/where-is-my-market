﻿using System.Collections;
using System.Text;

namespace Where_is_my_market.Helpers
{
    public class StringHelper
    {
        /// <summary>
        /// Builds one <see cref="string"/> from all elements of given <see cref="ICollection"/>, separated with given separator.
        /// </summary>
        /// <param name="collection">Collection of objects to convert.</param>
        /// <param name="separator"><see cref="string"/> value that is used to separate collection's elements. Single space by default.</param>
        /// <returns></returns>
        public static string BuildString(ICollection collection, string separator = " ")
        {
            StringBuilder strBuilder = new StringBuilder();

            if (collection.Count > 0)
            {
                foreach (var element in collection)
                {
                    strBuilder.Append(element.ToString() + separator);
                }

                strBuilder.Remove(strBuilder.Length - separator.Length, separator.Length);
            }

            return strBuilder.ToString();
        }


        /// <summary>
        /// Changes all polish special characters in given <see cref="string"/> to their equivalent in latin alphabet.
        /// </summary>
        /// <param name="text">Text to transform.</param>
        /// <returns></returns>
        public static string RemovePolishCharacters(string text)
        {
            StringBuilder strBuilder = new StringBuilder(text);
            char[] polishCharacters = { 'Ą', 'ą', 'Ć', 'ć', 'Ę', 'ę', 'Ł', 'ł', 'Ń', 'ń', 'Ó', 'ó', 'Ś', 'ś', 'Ż', 'ż', 'Ź', 'ź' };
            char[] normalCharacters = { 'A', 'a', 'C', 'c', 'E', 'e', 'L', 'l', 'N', 'n', 'O', 'o', 'S', 's', 'Z', 'z', 'Z', 'z' };

            for (int i = 0; i < polishCharacters.Length; i++)
            {
                strBuilder.Replace(polishCharacters[i], normalCharacters[i]);
            }

            return strBuilder.ToString();
        }


        /// <summary>
        /// Gets a substring located between given delimiters in <see cref="string"/>.
        /// </summary>
        /// <param name="originalString">A <see cref="string"/> that we want to search.</param>
        /// <param name="startSubstring">Start delimiter.</param>
        /// <param name="endSubstring">End delimiter.</param>
        /// <returns></returns>
        public static string SubstringBetween(string originalString, string startSubstring, string endSubstring)
        {
            string finalSubstring;

            try
            {
                int startIndex = originalString.IndexOf(startSubstring) + startSubstring.Length;
                int endIndex = originalString.IndexOf(endSubstring);

                finalSubstring = originalString.Substring(startIndex, endIndex - startIndex);
            }
            catch
            {
                finalSubstring = "";
            }

            return finalSubstring;
        }
    }
}
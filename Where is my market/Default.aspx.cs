﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Web.Services;
using System.Web.UI;
using Where_is_my_market.Helpers;
using Where_is_my_market.Models;

namespace Where_is_my_market
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }


        [WebMethod]
        public static string GetShopsNearby(double userLat, double userLng, double distance = 2)
        {
            List<Shop> nearShops = Shop.Near(new GeoCoordinate(userLat, userLng), distance);

            return WebHelper.ObjectToJson(nearShops);
        }

    }
}
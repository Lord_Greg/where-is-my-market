namespace Where_is_my_market.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Where_is_my_market.DAL.ShopContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Where_is_my_market.DAL.ShopContext";
        }

        protected override void Seed(Where_is_my_market.DAL.ShopContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.ChainsOfStores.AddOrUpdate(
                new ChainOfStores()
                {
                    ID = 1,
                    Name = "Biedronka",
                    MarkerColor = System.Drawing.Color.Red
                },
                new ChainOfStores()
                {
                    ID = 2,
                    Name = "Carrefour",
                    MarkerColor = System.Drawing.Color.Blue
                },
                new ChainOfStores()
                {
                    ID = 3,
                    Name = "Auchan",
                    MarkerColor = System.Drawing.Color.ForestGreen
                },
                new ChainOfStores()
                {
                    ID = 4,
                    Name = "Real"
                },
                new ChainOfStores()
                {
                    ID = 5,
                    Name = "Lidl",
                    MarkerColor = System.Drawing.Color.Yellow
                },
                new ChainOfStores()
                {
                    ID = 6,
                    Name = "�abka",
                    MarkerColor = System.Drawing.Color.Green
                },
                new ChainOfStores()
                {
                    ID = 7,
                    Name = "Polo Market",
                    MarkerColor = System.Drawing.Color.DarkRed
                }
            );

        }
    }
}

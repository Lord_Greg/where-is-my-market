﻿using FluentScheduler;
using System;
using System.Diagnostics;
using Where_is_my_market.Models;

namespace Where_is_my_market.Tasks
{
    public class Scheduler : Registry
    {
        public Scheduler()
        {
            WebScraper screper = new WebScraper();

            Schedule(() =>
            {
                string chainName = "Biedronka";
                ScrapeStartMessage(chainName);

                int actualizedCount = screper.GetBiedronkaShops();

                ScrapeEndMessage(chainName, actualizedCount);
            }).ToRunEvery(1).Months().OnTheFirst(DayOfWeek.Monday).At(0, 0);
            Schedule(() =>
            {
                string chainName = "Biedronka";
                GeoCodeGettingStartMessage(chainName);

                int actualizedCount = Shop.GetGeoCodes(chainName: chainName);

                GeoCodeGettingEndMessage(chainName, actualizedCount);
            }).ToRunEvery(1).Months().OnTheFirst(DayOfWeek.Monday).At(0, 15);
            
            Schedule(() =>
            {
                string chainName = "Carrefour";
                ScrapeStartMessage(chainName);

                int actualizedCount = screper.GetCarrefourShops();

                ScrapeEndMessage(chainName, actualizedCount);
            }).ToRunEvery(1).Months().OnTheFirst(DayOfWeek.Tuesday).At(0, 0);
            Schedule(() =>
            {
                string chainName = "Carrefour";
                GeoCodeGettingStartMessage(chainName);

                int actualizedCount = Shop.GetGeoCodes(chainName: chainName);

                GeoCodeGettingEndMessage(chainName, actualizedCount);
            }).ToRunEvery(1).Months().OnTheFirst(DayOfWeek.Tuesday).At(0, 15);

            Schedule(() =>
            {
                string chainName = "Auchan";
                ScrapeStartMessage(chainName);

                int actualizedCount = screper.GetAuchanShops();

                ScrapeEndMessage(chainName, actualizedCount);
            }).ToRunEvery(1).Months().OnTheFirst(DayOfWeek.Wednesday).At(0, 0);
            Schedule(() =>
            {
                string chainName = "Auchan";
                GeoCodeGettingStartMessage(chainName);

                int actualizedCount = Shop.GetGeoCodes(chainName: chainName);

                GeoCodeGettingEndMessage(chainName, actualizedCount);
            }).ToRunEvery(1).Months().OnTheFirst(DayOfWeek.Wednesday).At(0, 15);

            Schedule(() =>
            {
                string chainName = "Lidl";
                ScrapeStartMessage(chainName);

                int actualizedCount = screper.GetLidlShops();

                ScrapeEndMessage(chainName, actualizedCount);
            }).ToRunEvery(1).Months().OnTheFirst(DayOfWeek.Thursday).At(0, 0);
            Schedule(() =>
            {
                string chainName = "Lidl";
                GeoCodeGettingStartMessage(chainName);

                int actualizedCount = Shop.GetGeoCodes(chainName: chainName);

                GeoCodeGettingEndMessage(chainName, actualizedCount);
            }).ToRunEvery(1).Months().OnTheFirst(DayOfWeek.Thursday).At(0, 15);
        }


        private void ScrapeStartMessage(string chainName)
        {
            Debug.WriteLine("Begining actualization for {0} shops.", args: chainName);
        }

        private void ScrapeEndMessage(string chainName, int actializedCount)
        {
            Debug.WriteLine("Finished actualization for {0} shops.\nShops actialized: {1}.", chainName, actializedCount.ToString());
        }

        private void GeoCodeGettingStartMessage(string chainName)
        {
            Debug.WriteLine("Begining procedure of getting geo codes for {0} shops.", args: chainName);
        }

        private void GeoCodeGettingEndMessage(string chainName, int actializedCount)
        {
            Debug.WriteLine("Finished procedure of getting geo codes for {0} shops.\nShops actialized: {1}.", chainName, actializedCount.ToString());
        }
    }
}
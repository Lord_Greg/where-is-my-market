﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Where_is_my_market.DAL;
using Where_is_my_market.Helpers;

namespace Where_is_my_market.Models
{
    public class City
    {
        [ScaffoldColumn(false), JsonIgnore]
        public int ID { get; set; }

        [Required, Display(Name = "Nazwa")]
        public string Name { get; set; }


        [JsonIgnore]
        public virtual ICollection<Shop> Shops { get; set; }



        public City() { }

        public City(string name)
        {
            Name = name;
        }


        public static City FindOrCreate(string cityName, bool withoutPolishCharacters = false, bool allowCreate = true)
        {
            ShopContext db = new ShopContext();
            List<City> cities = db.Cities.ToList();
            City city;

            if (withoutPolishCharacters)
            {
                city = cities.Find(c => StringHelper.RemovePolishCharacters(c.Name) == StringHelper.RemovePolishCharacters(cityName));
            }
            else
            {
                city = cities.Find(c => c.Name == cityName);
            }

            // Add new city
            if (city == null && allowCreate)
            {
                city = new City(cityName);
                db.Cities.Add(city);
                db.SaveChanges();
            }

            return city;
        }

    }
}
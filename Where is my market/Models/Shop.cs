﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Device.Location;
using System.Linq;
using Where_is_my_market.DAL;
using Where_is_my_market.Helpers;
using Where_is_my_market.Tasks;

namespace Where_is_my_market.Models
{
    public class Shop
    {
        [ScaffoldColumn(false), JsonIgnore]
        public int ID { get; set; }

        [Required, Display(Name = "Nazwa Ulicy")]
        public string StreetName { get; set; }

        [Required, Display(Name = "Numer Ulicy")]
        public string StreetNo { get; set; }

        [Display(Name = "Długość Geograficzna")]
        public double? Longitude { get; set; }

        [Display(Name = "Szerokość Geograficzna")]
        public double? Latitude { get; set; }


        public virtual ChainOfStores Chain { get; set; }
        [JsonIgnore]
        public int ChainID { get; set; }

        public virtual City City { get; set; }
        [JsonIgnore]
        public int? CityID { get; set; }


        public Shop() { }

        public Shop(int chainID, string streetName, string streetNo, int? cityID)
        {
            ChainID = chainID;
            StreetName = streetName;
            StreetNo = streetNo;
            CityID = cityID;
        }

        public Shop(int chainID, string streetName, string streetNo, string cityName)
        {
            int cityID;
            ShopContext db = new ShopContext();
            IQueryable<City> cities = db.Cities.Where(city => city.Name == cityName);

            // Get city from DB
            if (cities.Count() > 0)
            {
                cityID = cities.First().ID;
            }
            // Create new city
            else
            {
                City newCity = new City() { Name = cityName };
                db.Cities.Add(newCity);
                db.SaveChanges();
                cityID = newCity.ID;
            }

            ChainID = chainID;
            StreetName = streetName;
            StreetNo = streetNo;
            CityID = cityID;
        }



        public bool GetGeoCode(bool actualizeInDb = false)
        {
            bool success = false;
            GeoCoordinate coordinate = GeoCoder.GetGeoCode(BuildAdressString());

            if (coordinate.IsUnknown)
            {
                success = false;
            }
            else
            {
                double? oldLat = Latitude;
                double? oldLng = Longitude;

                Latitude = coordinate.Latitude;
                Longitude = coordinate.Longitude;

                if (actualizeInDb)
                {
                    try
                    {
                        using (ShopContext db = new ShopContext())
                        {
                            db.Shops.Attach(this);

                            var entry = db.Entry(this);
                            entry.Property(property => property.Latitude).IsModified = true;
                            entry.Property(property => property.Longitude).IsModified = true;

                            db.SaveChanges();
                        }
                        success = true;
                    }
                    catch
                    {
                        Latitude = oldLat;
                        Longitude = oldLng;

                        success = false;
                    }
                }
                else
                {
                    success = true;
                }
            }

            return success;
        }



        private string BuildAdressString()
        {
            return StringHelper.BuildString(new List<string>() { StreetNo, StreetName, City.Name, "Polska" }, ", ");
        }



        public static List<Shop> Near(GeoCoordinate point, double distance = 2)
        {
            List<Shop> shops;
            GeoCoordinate[] borders = GeoHelper.GetDistanceBorderPoints(point, distance);
            GeoCoordinate borderNW = borders[0];
            GeoCoordinate borderSE = borders[1];

            using (ShopContext db = new ShopContext())
            {
                var query = db.GetShops(includeRelations: true).Where(shop =>
                    shop.Latitude <= borderNW.Latitude &&
                    shop.Latitude >= borderSE.Latitude &&
                    shop.Longitude >= borderNW.Longitude &&
                    shop.Longitude <= borderSE.Longitude
                );

                shops = query.ToList();
            }

            return shops;
        }

        public static int ActualizeDb(int chainID, List<Shop> newShops, bool keepOldShops = false)
        {
            int actualizedCount = 0;
            ShopContext db = new ShopContext();

            if (!keepOldShops)
            {
                db.Shops.RemoveRange(db.Shops.Where(shop => shop.ChainID == chainID));
                db.SaveChanges();
            }

            foreach (Shop newShop in newShops)
            {
                try
                {
                    db.Shops.Add(newShop);
                    db.SaveChanges();
                    actualizedCount++;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    db.Shops.Remove(newShop);
                }
            }

            return actualizedCount;
        }

        public static int GetGeoCodes(string chainName = "", string cityName = "", bool polishCharactersInCityName = true)
        {
            int actualizedCount = 0;
            List<Shop> shops = new List<Shop>();

            using (ShopContext db = new ShopContext())
            {
                shops = db.GetShops(includeRelations: true).ToList();

                if (chainName != "")
                {
                    shops = shops.Where(shop => shop.Chain.Name == chainName).ToList();
                }

                if (cityName != "")
                {
                    if (polishCharactersInCityName)
                    {
                        shops = shops.Where(shop => shop.City.Name == cityName).ToList();
                    }
                    else
                    {
                        shops = shops.Where(shop => StringHelper.RemovePolishCharacters(shop.City.Name) == StringHelper.RemovePolishCharacters(cityName)).ToList();
                    }
                }
            }

            foreach (Shop shop in shops)
            {
                if (shop.GetGeoCode(true))
                {
                    actualizedCount++;
                }
            }

            return actualizedCount;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using Where_is_my_market.DAL;
using Where_is_my_market.Models;

namespace Where_is_my_market
{
    public partial class Shops : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public List<Shop> GetShops([QueryString("chain")] string chain)
        {
            List<Shop> shops = new List<Shop>();

            using (ShopContext db = new ShopContext())
            {
                shops = db.GetShops(includeRelations: true).ToList();

                if (chain != null && chain != "")
                {
                    shops = shops.Where(shop => shop.Chain.Name.Contains(chain)).ToList();
                }

            }

            return shops;
        }
    }
}
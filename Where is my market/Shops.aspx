﻿<%@ Page Title="Sklepy" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Shops.aspx.cs" Inherits="Where_is_my_market.Shops" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Lista Sklepów</h1>

    <asp:ListView ID="shopList" runat="server"
        DataKeyNames="ID" ItemType="Where_is_my_market.Models.Shop"
        SelectMethod="GetShops" >
        
        <ItemTemplate>
            <li><%#:Item.Chain.Name%>&nbsp;&nbsp;&nbsp;<%#:Item.City.Name%> ul. <%#:Item.StreetName%> <%#:Item.StreetNo%></li>
        </ItemTemplate>
    </asp:ListView>

</asp:Content>
